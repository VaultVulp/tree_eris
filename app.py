#!/usr/bin/env python3


class Tree(dict):
    def __init__(self, iterable=None, **kwargs):
        super(Tree, self).__init__(**kwargs)
        if iterable:
            if hasattr(iterable, '__iter__'):
                for itm in iterable:
                    if isinstance(itm, tuple):
                        if len(itm) == 2:
                            self[itm[0]] = itm[1]
                        else:
                            raise ValueError
                    else:
                        self[itm] = iterable[itm]
            else:
                raise ValueError

    def __getitem__(self, k):
        if isinstance(k, str) and '.' in k:
            id = k.index('.')
            if id == 0 or id == len(k) - 1:
                raise KeyError
            return self[k[0:id]][k[id + 1:]]
        else:
            return self.setdefault(k, Tree())

    def __setitem__(self, *args, **kwargs):
        k, v = args

        if isinstance(k, str) and '.' in k:
            id = k.index('.')
            if id == 0 or id == len(k) - 1:
                raise KeyError
            self[k[0:id]][k[id + 1:]] = v
        else:
            return super().__setitem__(*args, **kwargs)

    def __eq__(self, other):
        return dict(self._plain_tree()) == other

    def __ne__(self, other):
        return dict(self._plain_tree()) != other

    def _plain_tree(self):
        """
        protected метод
        Возвращает плоское дерево для компаратора
        """
        for k in self.keys():
            yield (k, self[k])

    def keys(self, prefix=''):
        if prefix:
            prefix += '.'
        for k in super().keys():
            if isinstance(self[k], Tree):
                yield from self[k].keys(prefix + k)
            else:
                yield prefix + k

    def sorted_keys(self, prefix=''):
        """
        Альтернативная версия keys, возвращающая отсортированные по ключи
        :param prefix: префикс для рекурсивной обработки ключей
        """
        if prefix:
            prefix += '.'
        for k in sorted(super().keys()):
            if isinstance(self[k], Tree):
                yield from self[k].sorted_keys(prefix + k)
            else:
                yield prefix + k


if __name__ == '__main__':
    tree = Tree(one=1, two=2, three=3)
    print('Tree(one=1, two=2, three=3):', tree)
    tree = Tree({'th.ree': 3, 'one': 1, 'two': 2})
    print('Tree({\'th.ree\': 3, \'one\': 1, \'two\': 2}):', tree)
    tree = Tree([('t.w.o', 2), ('one', 1), ('three', 3)])
    print('Tree([(\'t.w.o\', 2), (\'one\', 1), (\'three\', 3)]:)', tree)
    tree = Tree(zip(['on.e', 'on.a', 'two', 'three'], [1, 5, 2, 3]))
    print('Tree(zip([\'on.e\', \'on.a\', \'two\', \'three\'], [1, 5, 2, 3])):',
          tree)
    print('------------')
    tree = Tree()
    tree['a.x'] = 1
    tree['a.y'] = 2
    tree['b']['x'] = 3
    tree['b']['y'] = 4

    print('tree:', tree)
    print('tree._plain_tree():', dict(tree._plain_tree()))
    print('tree[a]:', tree['a'])
    print('tree[b]:', tree['b'])
    print('tree[\'a.x\']:', tree['a.x'])
    print('tree[\'a\'][\'x\']:', tree['a']['x'])
    print('list(tree[\'a\'].keys()):', list(tree['a'].keys()))
    print('list(tree.keys()):', list(tree.keys()))
    print('list(tree.sorted_keys()):', list(tree.sorted_keys()))
    print('\'a.x\' in tree.keys():', 'a.x' in tree.keys())

    assert tree['a'] == {'x': 1, 'y': 2}

    assert tree['b'] == {'x': 3, 'y': 4}

    # Заменил list['x', 'y'] в assert'ах на set поскольку классический dict
    # возвращает неотсортированную коллекцию ключей.
    # Но я оставил альтернативный вариант с сортировкой - Tree.sorted_keys().
    # Cделал Tree.keys() и Tree.sorted_keys() генератороми, поскольку
    # внутри есть рекурсивные вызовы, в кейсах аналогичных 'a.x' in tree.keys().
    # это позволит снизить затраты по памяти и CPU
    assert set(tree['a'].keys()) == {'x', 'y'}

    assert set(tree.keys()) == {'a.x', 'a.y', 'b.x', 'b.y'}

    assert tree == {'a.x': 1, 'a.y': 2, 'b.x': 3, 'b.y': 4}
